<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05 Activity: Client Server Communication (Basic Task login/logout)</title>
</head>
<body>
    <?php session_start(); ?>

    <h1>s05 Activity</h1>
    <hr>

    <?php if(!isset($_SESSION['user'])): ?>

    <h2>Login</h2>
    <form method="POST" action="./login.php">
		<input type="hidden" name="action" value="login"/>
		email: <input type="email" name="email" required/>
        password: <input type="password" name="password" required/>
        <br><br>
		<button type="submit">Login</button>
	</form>
    
    <?php else: ?>
    
    <p>Hello, johnsmith@gmail.com</p>

    <form method="POST" action="./login.php">
		<input type="hidden" name="action" value="logout">
		<button type="submit">Logout</button>
	</form>

    <?php endif; ?>
</body>
</html>
